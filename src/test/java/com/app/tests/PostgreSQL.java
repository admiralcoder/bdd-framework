package com.app.tests;

import com.app.utilities.DBType;
import com.app.utilities.Database;
import com.app.utilities.dbUtils;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

public class PostgreSQL {

    @Test
    public void SetupConnection(){

        dbUtils a = new dbUtils();
        a.establishConnection(Database.POSTGRESQL);
        String query = "select * from accounts";
        a.getQueryResults("select * from accounts");
        List< Map<String,Object>> results = a.getQueryResults(query);
        System.out.println(results);

    }
}
