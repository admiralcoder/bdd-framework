package com.app.step_definitions;

import com.app.utilities.DBType;
import com.app.utilities.DBUtility;
import cucumber.api.java.en.Given;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class sqlFeature {

    @Given("^i set up connection$")
    public void i_set_up_connection() throws SQLException {
        DBUtility.establishConnection(DBType.ORACLE);
    }

    @Given("^I query database with sql statement \"([^\"]*)\"$")
    public void i_query_database_with_sql_statement(String sql) throws SQLException {
//        List<Map<String,Object>> DBDataList = DBUtility.runSQLQuery(sql);
        List<Map<String,Object>> empData = DBUtility
                .runSQLQuery(sql);

        System.out.println(empData.get(0));
    }

}
