package com.app.tests;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class InsertIntomySQL {
    public static void main(String ... args){
        InsertToDB();
    }

    private static void InsertToDB() {
        try {
//            String url = "jdbc:mysql://db4free.net:3306/tech?user=techlead&password=students";

            // create a mysql Database connection
            String myUrl = "jdbc:mysql://db4free.net:3306/tech";
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conn = DriverManager.getConnection(myUrl, "tech", "students");

            // the mysql insert statement
//            String query = " Insert into sampletable (FirstName, LastName , PhoneNo, Company) values(?,?,?,?)";

            String query2 = " INSERT INTO Students (name, lastname, team, student_id) VALUES (?, ?, ? , ?)";

            // create the mysql insert preparedstatement
            PreparedStatement preparedStmt = conn.prepareStatement(query2);
            preparedStmt.setString(1, "job");
            preparedStmt.setString(2, "johnes");
            preparedStmt.setString(3, "ninja turtles");
            preparedStmt.setInt(4, 505);

            // execute the preparedstatement
            preparedStmt.execute();
            conn.close();
        } catch (Exception e) {
            System.out.println("Got an exception!");
            System.out.println(e.getMessage());
        }
    }

}
