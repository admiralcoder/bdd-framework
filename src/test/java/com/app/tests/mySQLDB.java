package com.app.tests;

import com.app.utilities.DBConnectionManager;
import com.app.utilities.DBUtility;
import org.testng.annotations.Test;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class mySQLDB {
    public static void main(String ... args) throws SQLException {
        String q = "select * from students";

        //Option 1

        String [][] result = FetchDataFromDB(q);
        //Create a for loop to print 2 dimentional array else debug to see result
        System.out.println(result);

        //Option 2

        List<Map<String, Object>> results2 = runSQLQuery(q);
        System.out.println(results2);


    }


    @Test
    public void mysqlTest() throws SQLException {

//		-- Query the first 5 employees in the table
//		SELECT * FROM teachers;
        String query = "select * from teachers";

        List<Map<String,Object>> empData = runSQLQuery(query);

        System.out.println(empData);
    }


    public static String[][] FetchDataFromDB(String query) throws SQLException {

        String url = "jdbc:mysql://db4free.net:3306/user?user=tech&password=students";

        DBConnectionManager dbInstance = DBConnectionManager.getInstance(url);
        Connection conn = dbInstance.getConnection();
        Statement stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(query);

        int columnCount = rs.getMetaData().getColumnCount();

        rs.last();//this will move the result set to last row
        int rowcount = rs.getRow();// this will return the index
        rs.beforeFirst();// this will bring back to first row

        String [][] results = new String [rowcount][columnCount];

        int i = 0;
        while(rs.next()){
            for(int j = 0 ; j < columnCount; j++){
                results[i][j]=rs.getString(j+1);
            }
            i= i+1;
        }
        return results;
    }

    public static List<Map<String,Object>> runSQLQuery(String sql) throws SQLException{


        String url = "jdbc:mysql://db4free.net:3306/tech?user=techlead&password=students";

        DBConnectionManager dbInstance = DBConnectionManager.getInstance(url);
        Connection conn = dbInstance.getConnection();
        Statement stat = conn.createStatement();
        ResultSet rs = stat.executeQuery(sql);

        List<Map<String,Object>> list = new ArrayList<>();
        ResultSetMetaData rsMdata = rs.getMetaData();

        int colCount = rsMdata.getColumnCount();

        while(rs.next()) {
            Map<String,Object> rowMap = new HashMap<>();

            for(int col = 1; col <= colCount; col++) {
                rowMap.put(rsMdata.getColumnName(col), rs.getObject(col));
            }
            list.add(rowMap);
        }
        return list;

    }

}
