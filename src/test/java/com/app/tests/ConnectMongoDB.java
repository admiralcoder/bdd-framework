package com.app.tests;
import com.mongodb.MongoClient;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.mongodb.DB;
import org.omg.CORBA.portable.UnknownException;

public class ConnectMongoDB {

public static void main (String ... Args){
    try{
        // Set up connection to Mongo Client
        MongoClient mongoclient = new MongoClient("127.0.0.1",27017);
        System.out.println("Connection Successful");
        // Set up connection to Database
        DB db = mongoclient.getDB("local");
        // Set up connection to the Table
        DBCollection coll = db.getCollection("students");

        DBCursor cursor = coll.find();

        while(cursor.hasNext()){
            int i = 1;
            System.out.println(cursor.next());
            i++;
        }

    }catch (UnknownException e){
        e.printStackTrace();
    }
}
}
