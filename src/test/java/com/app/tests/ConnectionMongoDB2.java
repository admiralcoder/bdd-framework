package com.app.tests;

import com.mongodb.*;


    public class ConnectionMongoDB2 {

        /**
         * setting up connections to Mongo DB
         *
         * Performing CRUD OPPERATIONS
         *
         * @Author Alex Rodriguez
         *
         */

        public static void main(String... args) {
            // Connecting with server
            MongoClient mongoClient = new MongoClient("127.0.0.1",27017);
            System.out.println("server connection successful");

            // Connecting with database
            DB dbs = mongoClient.getDB("sports");
            System.out.println("Connection Successful");
            System.out.println("Database Name: "+ dbs.getName());

            System.out.println("Database stats: "+ dbs.getStats());

            System.out.println("Database get Collections: "+ dbs.getCollection("sports"));


            // Create collections
            DBCollection coll = dbs.getCollection("tech");
            System.out.println("Collections created successfully");

            // how to Insert in Collection created

//        BasicDBObject batch1 = new BasicDBObject("name","Juan").
//                append("age",23).
//                append("Batch",1).
//                append("Class","java").
//                append("Instructor","Beka");
//        coll.insert(batch1);
//
//        System.out.println("Document Inserted..");
//
//        BasicDBObject batch2 = new BasicDBObject("name","Steve").
//                append("age",45).
//                append("Batch",1).
//                append("Class","java").
//                append("Instructor","Beka");
//        coll.insert(batch2);
//
//        System.out.println("Document Inserted..");
//
//        BasicDBObject batch3 = new BasicDBObject("name","Stacy").
//                append("age",18).
//                append("Batch",1).
//                append("Class","java").
//                append("Instructor","Beka");
//        coll.insert(batch3);
//
//        System.out.println("Document Inserted..");

            // Once inserted uncoment insert lines


            Cursor cursor = coll.find();
            while(cursor.hasNext()){
                System.out.println(cursor.next());
            }

            //print just the name
//        Cursor cursor1 = coll.find();
//        while(cursor1.hasNext()) {
//            System.out.println(cursor1.next().get("name"));
//
//        }

            // get an specific name from the collection
//        BasicDBObject doc = new BasicDBObject("name","Stacy");
//        Cursor cursor2 = coll.find(doc);
//        while(cursor2.hasNext()){
//            System.out.println(cursor2.next());
//        }



            // update Existing Documents

            BasicDBObject updateDocument = new BasicDBObject();

            // new Values

            updateDocument.append("$set", new BasicDBObject().append("Instructor","Snoopy"));

            // Old Document

            BasicDBObject oldDocument = new BasicDBObject().append("name","Juan");

            //if you set second false to true it will update all the rows with same name
            coll.update(oldDocument,updateDocument,false,false);
            System.out.println("Document Updated Mongo DB....");

            cursor = coll.find();
            while(cursor.hasNext()){
                System.out.println(cursor.next());
            }


            //Delete one document from collection

            System.out.println("Deleting a document from collections");
            BasicDBObject del = new BasicDBObject("name", "Juan");
            coll.remove(del);
            cursor=coll.find();
            while(cursor.hasNext()){
                System.out.println(cursor.next());
            }


            // Drop Collection from database Delete Sports Collection or any collection

//            coll.drop();drop
//            System.out.println("Collection Dropped");
        }
    }


