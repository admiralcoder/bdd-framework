package com.app.tests;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.equalTo;
import static org.testng.Assert.assertEquals;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.testng.annotations.Test;
import com.app.utilities.ConfigurationReader;
import io.restassured.http.ContentType;
import io.restassured.response.Response;


public class APIDay3_GSON {
	@Test
	public void testWithPathParameter() {
		given().accept(ContentType.JSON)
				.when().get("http://www.google.com")
				.then().statusCode(200)
				.and().contentType(ContentType.JSON)
		);

	}
	
	@Test
	public void testWithJsonToHashMap() {
		Response response = given().accept(ContentType.JSON)
		.when().get(ConfigurationReader.getProperty("api"));
		
		Map<String,String> map =	response.as(HashMap.class);
		
		System.out.println(map.keySet());
		System.out.println(map.values());
		
		assertEquals(map.get("userId"),1);

	}
	// POST

	@Test
	public void postNewRegion() {
		String url = ConfigurationReader.getProperty("postapi");


		Map requestMap = new HashMap<>();
		requestMap.put("title", "test");
		requestMap.put("body", "this is the best class");
		requestMap.put("userId", 1);

		Response response = given().accept(ContentType.JSON)
				.and().contentType(ContentType.JSON)
				.and().body(requestMap)
				.when().post(url);

		System.out.println(response.statusLine());
		response.prettyPrint();

		//Then status code should be 201
		//And response body should match request body
		assertEquals(response.statusCode(), 201);
		Map responseMap = response.body().as(Map.class);

		//assertEquals(responseMap,requestMap); did not work
		assertEquals(responseMap.get("title"), requestMap.get("title"));
		assertEquals(responseMap.get("body"), requestMap.get("body"));

		//Assert body

		Map<String,String> map =response.as(HashMap.class);

		System.out.println(map.keySet());
		System.out.println(map.values());
		assertEquals(map.get("body"),"this is the best class");



	}




	public Map<Character, Integer> countLetters(String str) {    // if order is matter, we can use LinkedHashMap instead    Map<Character, Integer> letters = new HashMap<>();
		for(int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			// if map already contains the key, get the value and put back with +1
			// if(letters.containsKey(ch))
			// {        letters.put(ch, letters.get(ch) + 1);      }else {
			// if does not contains char as key, new letter put with value 1        letters.put(ch, 1);      }    }
			return letters;  }



//	@Test
//	public void convertJsonToListOfMaps() {
//		Response response = given().accept(ContentType.JSON)
//				.when().get(ConfigurationReader.getProperty("api1"));
//
//		//convert the response that contains department information into listof maps
//		//List<Map<String,String>>
//
//		//ArrayList<Map<String,String>> listOfMaps = response.as(ArrayList.class);
//		List<Map> listOfMaps = response.jsonPath().getList("posts/1",Map.class);
//
//		System.out.println(listOfMaps.get(0));
//
//		//assert that first department name is "Administration"
//
//		assertEquals(listOfMaps.get(0).get("id"),"1");
//	}


}
