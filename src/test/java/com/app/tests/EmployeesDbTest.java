package com.app.tests;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.app.utilities.DBType;
import com.app.utilities.DBUtility;

public class EmployeesDbTest {
	
	@BeforeClass
	public void setUp() throws SQLException {
		DBUtility.establishConnection(DBType.ORACLE);
	}
	
	@AfterClass
	public void tearDown() {
		 DBUtility.closeConnections();
	}
	
  @Test
  public void countTest() throws SQLException {
	  //Connect to oracle database
	  //run following sql query
	  //select * from employees where job_id = 'IT_PROG'
	  //more than 0 records should be returned
	  
	  int rowsCount = DBUtility.getRowsCount("select * from employees where job_id = 'IT_PROG'");
	  System.out.println(rowsCount);
	  assertTrue(rowsCount > 0);
  }
  
  @Test
  public void nameTestByID() throws SQLException {

	//Connect to oracle database
	//employees fullname with Employee id 105 should be David Austin
	 
	  List<Map<String,Object>> empData = DBUtility
			  .runSQLQuery("SELECT first_name,last_name FROM employees WHERE employee_id=105");

//	  List<Map<String,Object>> empData = DBUtility
//			  .runSQLQuery("SELECT * FROM employees");

	  System.out.println(empData.get(0));
	  
	  assertEquals(empData.get(0).get("FIRST_NAME"),"David");
	  assertEquals(empData.get(0).get("LAST_NAME"),"Austin");
  }

	@Test
	public void nameTestByID1() throws SQLException {

		//Connect to oracle database
		//employees fullname with Employee id 105 should be David Austin

		List<Map<String,Object>> empData = DBUtility
				.runSQLQuery("SELECT country_id from countries where country_id = 'BR'");

//	  List<Map<String,Object>> empData = DBUtility
//			  .runSQLQuery("SELECT * FROM employees");

//		System.out.println(empData);
		System.out.println(empData.get(0));

		assertEquals(empData.get(0).get("COUNTRY_ID"),"BR");
//		assertEquals(empData.get(0).get("LAST_NAME"),"Austin");
	}

	@Test
	public void countEmployees() throws SQLException {

//		-- write a query that will print the count of employees working in department id 100
//		select count(*)
//		from employees
//		where department_id=100;

		List<Map<String,Object>> empData = DBUtility
				.runSQLQuery("select count(*)\n" +
						"from employees\n" +
						"where department_id=100");

		System.out.println(empData.get(0));

	}

	@Test
	public void nameTestByID3() throws SQLException {

//		-- Query the first 5 employees in the table
//		SELECT * FROM EMPLOYEES WHERE EMPLOYEE_ID in (150,160)


		List<Map<String,Object>> empData = DBUtility
				.runSQLQuery("select *\n" +
						"from employees\n" +
						"where rownum <= 5");

		System.out.println(empData);
	}


	@Test
	public void nameTestByID4() throws SQLException {

//		Display details of employee with ID 150 or 160.


		List<Map<String,Object>> empData = DBUtility
				.runSQLQuery("SELECT * FROM EMPLOYEES WHERE EMPLOYEE_ID in (150,160)");

		System.out.println(empData);
	}

	@Test
	public void nameTestByID5() throws SQLException {

//		Display first name, salary, commission pct, and hire date for employees with salary less than 10000.
//		SELECT FIRST_NAME, SALARY, COMMISSION_PCT, HIRE_DATE FROM EMPLOYEES WHERE SALARY < 10000

		List<Map<String,Object>> empData = DBUtility
				.runSQLQuery("SELECT FIRST_NAME, SALARY, COMMISSION_PCT, HIRE_DATE FROM EMPLOYEES WHERE SALARY < 10000");

		System.out.println(empData);
	}


}









